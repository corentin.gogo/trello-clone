import React, { Component } from 'react'
import { connect } from 'react-redux'
import { DropTarget } from 'react-dnd'
import { bindActionCreators } from 'redux'

import { handleDrop, deleteList } from './../../actions'

import TaskForm from '../task/task-form'
import Task from '../task/task'
import './../../styles/list.css'

const dropSource = {
  drop (props, monitor) {
    const task = monitor.getItem()
    props.handleDrop(task.id, task.list, task.name, props.list.id)
  }
}

function collect (connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({shallow: true}),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType()
  }
}

class List extends Component {
  constructor () {
    super()
    this.handleDeleteList = this.handleDeleteList.bind(this)
  }

  renderTasks () {
    return this.props.tasks.map(task => task.list === this.props.list.id ? <Task key={task.id} taskId={task.id} /> : null)
  }

  handleDeleteList (e) {
    this.props.deleteList(this.props.list.id)
  }

  render () {
    return this.props.connectDropTarget(
      <div className='col-lg-3'>
        <div className='card'>
          <div className='card-header'>
            <h3>{this.props.list.name}</h3>
            <span onClick={this.handleDeleteList}>
              <i className='far fa-times-circle fa-lg delete' />
            </span>
          </div>
          <TaskForm list={this.props.list} />
          <ul className='list-group'>
            {this.renderTasks()}
          </ul>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    tasks: state.tasks
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({ handleDrop, deleteList }, dispatch)
}

const ListComponent = DropTarget('task', dropSource, collect)(List)

export default connect(mapStateToProps, mapDispatchToProps)(ListComponent)
