import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { addList } from './../../actions/index'

class ListForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: ''
    }
    this.submitList = this.submitList.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
  }

  submitList (e) {
    e.preventDefault()
    this.props.addList(this.state.name, this.props.board)
    this.setState({
      name: ''
    })
  }

  onInputChange (e) {
    this.setState({
      name: e.target.value
    })
  }

  render () {
    return (
      <form onSubmit={this.submitList}>
        <div className='form-group'>
          <input className='form-control' type='text' value={this.state.name} onChange={this.onInputChange} placeholder='Add a list' />
        </div>
      </form>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({ addList }, dispatch)
}

export default connect(null, mapDispatchToProps)(ListForm)
