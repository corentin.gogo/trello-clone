import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { addBoard } from '../../actions'
import '../../styles/board-form.css'

class BoardForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: ''
    }
    this.submitBoard = this.submitBoard.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
    this.onCancel = this.onCancel.bind(this)
  }

  submitBoard (e) {
    e.preventDefault()
    this.props.addBoard(this.state.name)
    this.setState({
      name: ''
    })
    this.props.history.push('/boards')
  }

  onInputChange (e) {
    this.setState({
      name: e.target.value
    })
  }

  onCancel (e) {
    e.preventDefault()
    this.props.history.push('/')
  }

  render () {
    return (
      <div className='card board-form col-lg-4'>
        <div className='card-header'>
          <h2>Create Board</h2>
        </div>
        <form onSubmit={this.submitBoard}>
          <div className='form-group'>
            <label htmlFor='board-name'>Choose a board name</label>
            <input className='form-control' type='text' value={this.state.name} onChange={this.onInputChange} />
            <button className='btn btn-danger' onClick={this.onCancel}>Cancel</button>
            <button type='submit' className='btn btn-primary'>Create</button>
          </div>
        </form>
      </div>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({ addBoard }, dispatch)
}

export default connect(null, mapDispatchToProps)(BoardForm)
