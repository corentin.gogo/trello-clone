import React, { Component } from 'react'
import { connect } from 'react-redux'
import { DragDropContext } from 'react-dnd'
import HTML5BackEnd from 'react-dnd-html5-backend'

import ListForm from '../list/list-form'
import List from '../list/list'

import './../../styles/board.css'

class Board extends Component {
  renderLists () {
    return this.props.lists.map(list => (list.board === this.props.board._id) ? <List key={list.id} list={list} /> : null)
  }

  render () {
    return (
      <div className='card'>
        <div className='card-header'>
          <h2>{this.props.board.title}</h2>
        </div>
        <ListForm board={this.props.board._id} />
        <div className='container'>
          <div className='row'>
            {this.renderLists()}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    board: state.boards.find(board => board._id === ownProps.match.params.id),
    lists: state.lists
  }
}

const BoardComponent = DragDropContext(HTML5BackEnd)(Board)
export default connect(mapStateToProps)(BoardComponent)
