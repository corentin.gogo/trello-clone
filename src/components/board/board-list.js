import React, { Component } from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import { Link } from 'react-router-dom'

import { deleteBoard, fetchBoards } from './../../actions'

import './../../styles/board-list.css'

class BoardList extends Component {
  constructor () {
    super()
    this.handleDeleteBoard = this.handleDeleteBoard.bind(this)
  }

  componentDidMount () {
    this.props.fetchBoards()
  }

  handleDeleteBoard (idBoard, event) {
    event.preventDefault()
    this.props.deleteBoard(idBoard)
  }

  renderBoardList () {
    return this.props.boards.map(
      board => {
        return (
          <li key={board._id}>
            <div className='btn-group' role='group'>
              <Link to={`board/${board._id}`} className='btn btn-primary btn-lg board'>{board.title}</Link>
              <button type='button' className='btn btn-danger btn-lg board' onClick={this.handleDeleteBoard.bind(this, board._id)}>
                <i className='fa fa-trash' />
              </button>
            </div>
          </li>
        )
      }
    )
  }
  render () {
    return (
      <div>
        {this.renderBoardList()}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    boards: state.boards
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({ deleteBoard, fetchBoards }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(BoardList)
