import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { addTask } from './../../actions'

class TaskForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: ''
    }

    this.submitTask = this.submitTask.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
  }

  submitTask (e) {
    e.preventDefault()
    this.props.addTask(this.state.name, this.props.list.id)
    this.setState({
      name: ''
    })
  }

  onInputChange (e) {
    this.setState({
      name: e.target.value
    })
  }

  render () {
    return (
      <form onSubmit={this.submitTask}>
        <div className='form-group'>
          <input type='text' className='form-control' value={this.state.name} onChange={this.onInputChange} placeholder='Add a task' />
        </div>
      </form>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({ addTask }, dispatch)
}

export default connect(null, mapDispatchToProps)(TaskForm)
