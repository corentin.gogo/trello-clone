import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { DragSource } from 'react-dnd'

import { toggleTask, deleteTask } from './../../actions'

import './../../styles/task.css'

const taskSource = {
  beginDrag (props) {
    return {
      id: props.task.id,
      name: props.task.name
    }
  }
}

function collect (connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

class Task extends Component {
  constructor () {
    super()
    this.toggleTask = this.toggleTask.bind(this)
    this.handleDeleteTask = this.handleDeleteTask.bind(this)
  }

  toggleTask (e) {
    this.props.toggleTask(this.props.task.id)
  }

  handleDeleteTask (e) {
    this.props.deleteTask(this.props.task.id)
  }

  render () {
    const check = this.props.task.done ? (<span><i className='fas fa-check' /></span>) : null
    return this.props.connectDragSource(
      <li className='list-group-item' onClick={this.toggleTask}>
        <span>{this.props.task.name}</span> {check}
        <span onClick={this.handleDeleteTask}>
          <i className='far fa-times-circle fa-lg task' />
        </span>
      </li>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({ toggleTask, deleteTask }, dispatch)
}

function mapStateToProps (state, ownProps) {
  return {
    task: state.tasks.filter(task => task.id === ownProps.taskId)[0]
  }
}

const TaskComponent = DragSource('task', taskSource, collect)(Task)

export default connect(mapStateToProps, mapDispatchToProps)(TaskComponent)
