import { ADD_BOARD, ADD_LIST, DELETE_LIST, ADD_TASK, TOGGLE_TASK, DELETE_TASK, HANDLE_DROP, DELETE_BOARD, FETCH_BOARDS } from './actionTypes'

import trelloService from '../services/trello-service'

export const fetchBoards = async () => {
  const boards = await trelloService.Get('/boards')
  return {
    type: FETCH_BOARDS,
    boards: boards
  }
}

export const addBoard = async (name) => {
  let board = { title: name }
  board = await trelloService.Post('/boards', board)
  return {
    type: ADD_BOARD,
    board
  }
}

export const deleteBoard = async (idBoard) => {
  const board = await trelloService.Delete('/boards/' + idBoard)
  return {
    type: DELETE_BOARD,
    board
  }
}

let listId = 0
export const addList = (name, board) => {
  return {
    type: ADD_LIST,
    id: ++listId,
    name,
    board
  }
}

export const deleteList = (idList) => {
  return {
    type: DELETE_LIST,
    idList
  }
}

let taskId = 0
export const addTask = (name, list) => {
  return {
    type: ADD_TASK,
    id: ++taskId,
    list,
    name,
    done: false
  }
}

export const toggleTask = (idTask) => {
  return {
    type: TOGGLE_TASK,
    idTask
  }
}

export const deleteTask = (idTask) => {
  return {
    type: DELETE_TASK,
    idTask
  }
}

export const handleDrop = (idTask, oldListId, nameTask, newListId) => {
  return {
    type: HANDLE_DROP,
    idTask,
    oldListId,
    nameTask,
    newListId
  }
}
