export const FETCH_BOARDS = 'FETCH_BOARDS'
export const ADD_BOARD = 'ADD_BOARD'
export const DELETE_BOARD = 'DELETE_BOARD'

export const ADD_LIST = 'ADD_LIST'
export const DELETE_LIST = 'DELETE_LIST'

export const ADD_TASK = 'ADD_TASK'
export const TOGGLE_TASK = 'TOGGLE_TASK'
export const DELETE_TASK = 'DELETE_TASK'

export const HANDLE_DROP = 'HANDLE_DROP'
