import { ADD_TASK, TOGGLE_TASK, DELETE_TASK, HANDLE_DROP } from './../actions/actionTypes'

const taskReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_TASK:
      return [
        ...state,
        {
          id: action.id,
          name: action.name,
          list: action.list,
          done: action.done
        }
      ]
    case TOGGLE_TASK:
      return state.map(task => (task.id === action.idTask) ? {...task, done: !task.done} : task)
    case DELETE_TASK:
      const newState = state.slice()
      const indexDelete = state.findIndex(task => task.id === action.idTask)
      newState.splice(indexDelete, 1)
      return newState
    case HANDLE_DROP:
      return state.map(task => {
        const newTask = {
          ...task,
          list: action.newListId
        }
        return (task.id === action.idTask) ? newTask : task
      })
    default:
      return state
  }
}

export default taskReducer
