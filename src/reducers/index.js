import { combineReducers } from 'redux'

import boardReducer from './board-reducer'
import listReducer from './list-reducer'
import taskReducer from './task-reducer'

const reducers = combineReducers({
  boards: boardReducer,
  lists: listReducer,
  tasks: taskReducer
})

export default reducers
