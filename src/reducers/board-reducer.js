import { ADD_BOARD, DELETE_BOARD, FETCH_BOARDS } from '../actions/actionTypes'

const boardReducer = (state = [], action) => {
  switch (action.type) {
    case FETCH_BOARDS:
      state = action.boards
      return state

    case ADD_BOARD:
      return [
        ...state,
        action.board
      ]
    case DELETE_BOARD:
      const newState = state.slice()
      const indexDelete = state.findIndex(board => board._id === action.board._id)
      newState.splice(indexDelete, 1)
      return newState
    default:
      return state
  }
}

export default boardReducer
