import { ADD_LIST, DELETE_LIST } from './../actions/actionTypes'

const listReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_LIST:
      return [
        ...state,
        {
          id: action.id,
          name: action.name,
          board: action.board
        }
      ]
    case DELETE_LIST:
      const newState = state.slice()
      const indexDelete = state.findIndex(list => list.id === action.idList)
      newState.splice(indexDelete, 1)
      return newState
    default:
      return state
  }
}

export default listReducer
