import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'

import BoardForm from './components/board/board-form'
import BoardList from './components/board/board-list'
import Board from './components/board/board'

import './styles/App.css'

class App extends Component {
  render () {
    return (
      <Router>
        <div>
          <Link className='btn btn-success nav-link' to='/board'>New Board</Link>
          <Link className='btn btn-success nav-link' to='/boards'>Boards</Link>
          <Switch>
            <Route path='/board/:id' component={Board} />
            <Route path='/board' component={BoardForm} />
            <Route path='/boards' component={BoardList} />
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App
