import axios from 'axios'

const API_URL = 'http://localhost:8080/api'

class TrelloService {
  static async Get (uri) {
    const res = await axios.get(API_URL + uri)
    return res.data
  }

  static async Post (uri, board) {
    const res = await axios.post(API_URL + uri, board)
    return res.data
  }

  static async Delete (uri) {
    const res = await axios.delete(API_URL + uri)
    return res.data
  }
}

export default TrelloService
